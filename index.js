'use strict';

module.exports.AssignExtension = require('./lib/assign');
module.exports.GroupByExtension = require('./lib/groupBy');
module.exports.ImportConfigExtension = require('./lib/importConfig');
module.exports.LinkBuildersExtension = require('./lib/linkBuilders');
module.exports.MergeExtension = require('./lib/merge');
