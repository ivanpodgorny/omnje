'use strict';

const _groupBy = require('lodash/groupBy');

const CustomTag = require('./CustomTag');

function GroupByExtension() {
  CustomTag.call(this, 'group_by', 'as', 'string');
}

GroupByExtension.prototype.run = function(context, args) {
  let arr = args[this.keyWordNames.PARAM][0];
  let prop = args[this.keyWordNames.PARAM][1];
  let alias = args[this.keyWordNames.TARGET];

  context.ctx[alias] = _groupBy(arr, prop);
}

GroupByExtension.prototype.__proto__ = CustomTag.prototype;

module.exports = GroupByExtension;
