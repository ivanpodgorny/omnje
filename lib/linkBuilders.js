'use strict';

const CustomTag = require('./CustomTag');

function LinkBuildersExtension(env) {
  CustomTag.call(this, 'link_builders', 'as', 'string');

  this.templateConsts = env.globals.templateConsts;
}

LinkBuildersExtension.prototype.run = function(context, args) {
  let builders = args[this.keyWordNames.PARAM];
  let buildersAlias = args[this.keyWordNames.TARGET];
  let buildersKeys = [
    this.templateConsts.HEADER,
    this.templateConsts.MAIN,
    this.templateConsts.FOOTER
  ];
  let buildersStore = {};

  for (let i = 0; i < builders.length; i++) {
    if (!builders[i]) continue;

    buildersStore[buildersKeys[i]] = builders[i];
  }

  context.ctx[buildersAlias] = buildersStore;
}

LinkBuildersExtension.prototype.__proto__ = CustomTag.prototype;

module.exports = LinkBuildersExtension;
