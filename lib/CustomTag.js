'use strict';

function CustomTag(tagName, delimiter, targetType) {
  this.tags = [tagName];

  this.keyWordNames = {
    PARAM: 'param',
    TARGET: 'target',
    DATA: 'data',
    DELIMITER: delimiter
  }

  this.argTypes = {
    STRING: 'string',
    EXPRESSION: 'expression'
  }

  this.targetType = targetType;
}

CustomTag.prototype.parse = function(parser, nodes, lexer) {
  let tok = parser.nextToken();
  let args = new nodes.Array(tok.lineno, tok.colno);

  let template = parser.parseExpression();

  let target, targetNode;

  if (parser.skipSymbol(this.keyWordNames.DELIMITER)) {
    if (this.targetType == this.argTypes.STRING) {
      target = parser.parsePrimary().value;
      targetNode = new nodes.Literal(template.lineno, template.colno, target);
    } else {
      target = parser.parseExpression();
      targetNode = target;
    }
  } else {
    parser.fail(this.tags[0] + ': expected ' + this.keyWordNames.DELIMITER);
  }

  let dict = new nodes.Dict(template.lineno, template.colno);
  dict.addChild(new nodes.Pair(
    template.lineno, template.colno,
    new nodes.Literal(template.lineno, template.colno, this.keyWordNames.PARAM), template
  ));
  dict.addChild(new nodes.Pair(
    template.lineno, template.colno,
    new nodes.Literal(template.lineno, template.colno, this.keyWordNames.TARGET),
    targetNode
  ));
  args.addChild(dict);

  let nextTok = parser.peekToken();
  if(nextTok.type === lexer.TOKEN_BLOCK_END) {
    parser.nextToken();
  }

  return new nodes.CallExtension(this, 'run', args);
}

module.exports = CustomTag;
