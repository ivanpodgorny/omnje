'use strict';

const CustomTag = require('./CustomTag');

function AssignExtension() {
  CustomTag.call(this, 'assign', 'to', 'expression');
}

AssignExtension.prototype.run = function(context, args) {
  Object.assign(args[this.keyWordNames.TARGET], args[this.keyWordNames.PARAM]);
}

AssignExtension.prototype.__proto__ = CustomTag.prototype;

module.exports = AssignExtension;
