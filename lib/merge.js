'use strict';

const _merge = require('lodash/merge');

const CustomTag = require('./CustomTag');

function MergeExtension() {
  CustomTag.call(this, 'merge', 'with', 'expression');
}

MergeExtension.prototype.run = function(context, args) {
  _merge(args[this.keyWordNames.PARAM], args[this.keyWordNames.TARGET]);
}

MergeExtension.prototype.__proto__ = CustomTag.prototype;

module.exports = MergeExtension;
