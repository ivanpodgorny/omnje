'use strict';

const path = require('path');
const _reduce = require('lodash/reduce');
const CustomTag = require('./CustomTag');

function ImportConfigExtension(env, prefix) {
  CustomTag.call(this, 'import_config', 'as', 'string');

  this.env = env;
  this.templateConsts = this.env.globals.templateConsts;
  this.prefix = prefix;
}

ImportConfigExtension.prototype.run = function(context, args) {
  const __this = this;

  let dataFile = args[this.keyWordNames.PARAM];
  let dataAlias = args[this.keyWordNames.TARGET];

  let config = this.parseConfig(dataFile);
  if (!config.attrs) {
    config.attrs = {};
  }
  context.ctx[dataAlias] = config;

  this.addConfigModifier('mods', context, function(mods) {
    let config = context.ctx[dataAlias];

    if (mods) {
      config.mods = mods;
    } else if (config.mods) {
      delete config.mods;
    }

    __this.pushClass(config);

    return this;
  });
}

ImportConfigExtension.prototype.parseConfig = function(dataFile) {
  let jsonData = this.parseJSON(dataFile);
  if (jsonData.type == this.templateConsts.ELEMENT) {
    this.pushParentName(dataFile, jsonData);
  }
  this.pushClass(jsonData);

  return jsonData;
}

ImportConfigExtension.prototype.pushClass = function(paramsObj) {
  let className;

  switch (paramsObj.type) {
    case this.templateConsts.BLOCK:
      className = this.prefix + paramsObj.name;
      break;
    case this.templateConsts.ELEMENT:
      className = this.prefix + paramsObj.parent + '__' + paramsObj.name;
      break;
    default:
      className = '';
  }

  className = paramsObj.mods ? this.parseMods(paramsObj.mods, className) : className;

  className = paramsObj.mix ? this.prefix + paramsObj.mix + ' ' + className : className;

  className = paramsObj.cls ? className + ' ' + paramsObj.cls : className;

  if (paramsObj.class) {
    paramsObj.class = className.trim()
  } else if (className) {
    Object.assign(paramsObj, {class: className.trim()});
  }
}

ImportConfigExtension.prototype.parseMods = function(mods, className) {
  let modsClass;
  let modsClassesArr = _reduce(mods, function(result, value, key) {
    modsClass = (value == true) ? className + '_' + key : className + '_' + key + '_' + value;
    result.push(modsClass);
    return result;
  }, [])

  return className + ' ' + modsClassesArr.join(' ');
}

ImportConfigExtension.prototype.pushParentName = function(filePath, context) {
  let pathInfo = path.parse(filePath);
  let fileName = pathInfo.base;
  let dirName = path.dirname(pathInfo.dir);
  let parentName = this.parseJSON(dirName + '/' + fileName).name;

  Object.assign(context, {parent: parentName});
}

ImportConfigExtension.prototype.parseJSON = function(filePath) {
  return JSON.parse(this.env.loaders[0].getSource(filePath).src);
}

ImportConfigExtension.prototype.addConfigModifier = function(name, context, modifierFunc) {
  context.exported.push(name);
  context.ctx[name] = modifierFunc;
}

ImportConfigExtension.prototype.__proto__ = CustomTag.prototype;

module.exports = ImportConfigExtension;
